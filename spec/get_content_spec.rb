require "GetPriceCar/get_content"


describe GetContent do

  describe "#get" do
    context "given an url return html content" do
      it "should return content" do

        getContent = GetContent.new
        url = "http://www.supercarros.com/buscar/?do=1&ObjectType=1&Brand=26&Model=122&Edition=1589&PriceFrom=0&PriceTo=20000000&YearFrom=2004&YearTo=2004"
        expect(getContent.get url).not_to be nil
      end
    end
  end
end