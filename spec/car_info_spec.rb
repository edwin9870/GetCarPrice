require "GetPriceCar/car_info"


describe CarInfo do

  describe "#getCars" do
    context "given a url " do
      it "check if the return is an array" do
        carInfo = CarInfo.new
        url = "http://www.supercarros.com/buscar/?do=1&ObjectType=1&Brand=26&Model=122&Edition=1589&PriceFrom=0&PriceTo=20000000&YearFrom=2004&YearTo=2004"
        carsInfoResponse = carInfo.getCars url
        expect(carsInfoResponse.is_a?(Array)).to be true
      end

      it "check if the return price is bigger than zero" do
        carInfo = CarInfo.new
        url = "http://www.supercarros.com/buscar/?do=1&ObjectType=1&Brand=26&Model=122&Edition=1589&PriceFrom=0&PriceTo=20000000&YearFrom=2004&YearTo=2004"
        carsInfoResponse = carInfo.getCars url
        puts "Car price: #{carsInfoResponse[0].price}"
        expect(carsInfoResponse[0].price).to be > 0
      end

      it "check if the return contain a car name" do
        carInfo = CarInfo.new
        url = "http://www.supercarros.com/buscar/?do=1&ObjectType=1&Brand=26&Model=122&Edition=1589&PriceFrom=0&PriceTo=20000000&YearFrom=2004&YearTo=2004"
        carsInfoResponse = carInfo.getCars url
        puts "Car name: #{carsInfoResponse[0].name}"
        expect(carsInfoResponse[0].name.size).to be > 0
      end

      it "check if all array elements of return contain a car name" do
        carInfo = CarInfo.new
        url = "http://www.supercarros.com/buscar/?do=1&ObjectType=1&Brand=26&Model=122&Edition=1589&PriceFrom=0&PriceTo=20000000&YearFrom=2004&YearTo=2004"
        carsInfoResponse = carInfo.getCars url
        allElementContainsName = carsInfoResponse.all? {|car| car.name.size > 0}
        puts "Variable allElementContainsName value: #{allElementContainsName}"

        expect(allElementContainsName).to be true
      end
      it "check if all return prices is bigger than zero" do
        carInfo = CarInfo.new
        url = "http://www.supercarros.com/buscar/?do=1&ObjectType=1&Brand=26&Model=122&Edition=1589&PriceFrom=0&PriceTo=20000000&YearFrom=2004&YearTo=2004"
        carsInfoResponse = carInfo.getCars url
        allElementContainsPrice = carsInfoResponse.all? {|car| car.price > 0}
        puts "Variable allElementContainsPrice value: #{allElementContainsPrice}"
        expect(allElementContainsPrice).to be true
      end
      it "check if the return contain a concurrency value" do
        carInfo = CarInfo.new
        url = "http://www.supercarros.com/buscar/?do=1&ObjectType=1&Brand=26&Model=122&Edition=1589&PriceFrom=0&PriceTo=20000000&YearFrom=2004&YearTo=2004"
        carsInfoResponse = carInfo.getCars url
        allElementContainsConcurrency = carsInfoResponse.all? {|car| car.concurrency.size > 0}
        puts "Concurrency value of first element: #{carsInfoResponse[0].concurrency}"
        expect(allElementContainsConcurrency).to be true
      end
    end
  end

  describe "#getCarMean" do
    context "given a list of car " do
      it "Check that the method return a array list" do
        carInfo = CarInfo.new
        car1 = Car.new
        car1.name = "Toyota Corolla LE"
        car1.year = "2004"
        car1.price = 395000
        car1.concurrency = "RD$"

        car2 = Car.new
        car2.name = "Toyota Corolla LE"
        car2.year = "2004"
        car2.price = 392000
        car2.concurrency = "RD$"

        car3 = Car.new
        car3.name = "Toyota Corolla S"
        car3.year = "2004"
        car3.price = 390000
        car3.concurrency = "RD$"

        cars = [car1, car2, car3]
        carsMean = carInfo.getCarMean(cars)
        p carsMean
        expect(carsMean.is_a?(Array)).to be true
      end
      it "check that the mean price is correct" do 
        carInfo = CarInfo.new
        car1 = Car.new
        car1.name = "Toyota Corolla LE"
        car1.year = "2004"
        car1.price = 395000
        car1.concurrency = "RD$"

        car2 = Car.new
        car2.name = "Toyota Corolla LE"
        car2.year = "2004"
        car2.price = 391200
        car2.concurrency = "RD$"

        meanPrice = 393100.0

        cars = [car1, car2]
        carsMean = carInfo.getCarMean cars
        p carsMean
        expect(carsMean[0].price).to equal(meanPrice)
      end
    end
  end
end