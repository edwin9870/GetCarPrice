require "httpclient"
require 'logger'

class GetContent

  def initialize 
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::WARN
  end

  def get(url)
    @logger.info "url...: #{url}"
    http = HTTPClient.new
    content =  http.get_content(url)
    return content
  end

end