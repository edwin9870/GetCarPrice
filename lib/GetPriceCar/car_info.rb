require 'logger'
require_relative "get_content"
require_relative "car"
require 'nokogiri'

class CarInfo

  def initialize 
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::WARN
  end

  def getCars(url) 
    getContent = GetContent.new
    pageContent = getContent.get(url)

    @logger.debug pageContent

    html_doc = Nokogiri::HTML(pageContent)

    carResult = html_doc.css("div#bigsearch-results-inner-results ul li")
    result = carResult.map {|carHTML|
      car = Car.new
      car.name = carHTML.css("div.title1").text
      car.year = carHTML.css("div.year").text
      car.price = Integer(carHTML.css("div.price").text[/[\d\,]+/].gsub(/,/,""))
      car.concurrency = carHTML.css("div.price").text[/[\w\$]+/]

      if car.concurrency == 'US$'
        car.price = car.price * 47
        car.concurrency = 'RD$'
      end
      car
    }

    return result.compact
  end

  def getCarMean(cars) 
    carsResult = cars.group_by {|car| car.name + car.year}.map{|k,v| 
      carPriceList = v.map{|car| car.price}
      carPriceMean = carPriceList.reduce {|sum, obj| sum + obj}.to_f / v.size
      car = Car.new
      car.name = v[0].name
      car.year = v[0].year
      car.price = carPriceMean
      car.concurrency = v[0].concurrency

      car
    }
    carsResult


  end

end